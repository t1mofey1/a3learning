FROM node:current-alpine

WORKDIR /app

COPY index.js ./
COPY package.json ./
COPY yarn.lock ./

RUN yarn

CMD ["node", "index"]