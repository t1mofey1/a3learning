const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

const json = bodyParser.json();

app.use(json);
app.use(cors());

app.post('/ping', (req, res) => {
  if (req.body.pong) {
    return res.json({
      pong: 'ping',
    })
  }
  res.json({});
});

app.get('/', (req, res) => {
  res.send('hi')
})

app.listen(9999, '0.0.0.0', () => {
  console.log('Server up')
})